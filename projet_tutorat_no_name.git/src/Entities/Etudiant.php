<?php


namespace Entities;

use DateTime;

class Etudiant
{
    private $Num_Etu = null;
    private $Nom_Etu;
    private $Pre_Etu;
    private $Tel_Etu;
    private $Mai_Etu;
    private $Log_Etu;
    private $Mdp_Etu;
    private $Rem_Etu;
    private $Dat_Pnt_Tel_Etu;
    private $Suj_Etu;
    private $Id_Cla;
    private $Id_Spe;
    private $Num_Tut;
    private $Id_Sta;
    private $Id_Ent;
    private $Note_Etu;

    function getNum_Etu() : ?int
{
    return $this -> Num_Etu;
}

function getNom_Etu() : string
{
    return $this -> Nom_Etu;
}

function getPre_Etu() : string
{
    return $this -> Pre_Etu;

}

function getTel_Etu() : string
{
    return $this->Tel_Etu;
}

function getMai_Etu() : string
{
    return $this->Mai_Etu;
}

function getLog_Etu() : string
{
    return $this->Log_Etu;
}

function getMdp_Etu() : string
{
    return $this-> Mdp_Etu;
}

function getRem_Etu() : string
{
    return $this->Rem_Etu;
}

function getDat_Pnt_Tel_Etu () : string
{
    return $this-> Dat_Pnt_Tel_Etu;
}


function getSuj_Etu () : string
{
    return $this -> Suj_Etu;
}

function getId_Cla () : ?int
{
    return $this->Id_Cla;
}

function getId_Spe () : ?int
{
    return $this->Id_Spe;
}

function getNum_Tut () : ?int
{
    return $this->Num_Tut;
}
function getId_Sta() : int
{
    return $this->Id_Sta;
}
function getId_Ent() : ?int
{
    return $this->Id_Ent;
}

function getNote_Etu() : ?int
{
    return $this->Note_Etu;
}







function setNum_Etu(int $Num_Etu): self
{
    if($this->Num_Etu == Null){
        $this->Num_Etu = ($tmp = filter_var($Num_Etu, FILTER_VALIDATE_INT)) ? $tmp : null;
    }
    return $this;

}

function setNom_Etu(string $Nom_Etu): self
{
    $this->Nom_Etu = $Nom_Etu;
    return $this;
}

function setPre_Etu(string $Pre_Etu): self
{
    $this -> Pre_Etu = $Pre_Etu;
    return $this;
}

function setTel_Etu(string $Tel_Etu): self
{
    $this->Tel_Etu = $Tel_Etu;
    return $this;
}


function setMai_Etu(string $Mai_Etu): self
{
    $this->Mai_Etu = $Mai_Etu;
    return $this;
}

function setLog_Etu(string $Log_Etu): self
{
    $this-> Log_Etu = $Log_Etu;
    return $this;
}

function setMdp_Etu(string $Mdp_Etu): self
{
    $this->Mdp_Etu = $Mdp_Etu;
    return $this;
}

function setRem_Etu(string $Rem_Etu): self
{
    $this->Rem_Etu = $Rem_Etu;
    return $this;
}

function setDat_Pnt_Tel_Etu(string $Dat_Pnt_Tel_Etu): self
{
    $this->Dat_Pnt_Tel_Etu = $Dat_Pnt_Tel_Etu;
    return $this;   
}

 function setSuj_Etu(string $Suj_Etu): self
 {
    $this->Suj_Etu= $Suj_Etu;
     return $this;
}

function setId_Cla(int $Id_Cla): self
{
        $this->Id_Cla= $Id_Cla;
        return $this;
}

function setId_Spe(int $Id_Spe): self
{
    $this->Id_Spe= $Id_Spe;
    return $this;
}

function setNum_Tut(int $Num_Tut): self
{
    $this->Num_Tut= $Num_Tut;
    return $this;
}

function setId_Sta(int $Id_Sta): self {
        $this->Id_Sta= $Id_Sta;
        return $this;
    }

function setId_Ent(int $Id_Ent): self {
        $this->Id_Ent= $Id_Ent;
        return $this;
    }

    function setNote_Etu(int $Note_Etu): self {
        $this -> Note_Etu = $Note_Etu;
        return $this;
    }



function __construct(?array $datas = null){

    if(!is_null($datas)){
        (isset($datas['Num_Etu'])) ? $this->setNum_Etu($datas['Num_Etu'] ): $this->Num_Etu=null;
        (isset($datas['Nom_Etu'])) ? $this->setNom_Etu($datas['Nom_Etu'] ): $this->setNom_Etu('');
        (isset($datas['Pre_Etu'])) ? $this->setPre_Etu($datas['Pre_Etu'] ): $this->setPre_Etu('');
        (isset($datas['Tel_Etu'])) ? $this->setTel_Etu($datas['Tel_Etu'] ): $this->setTel_Etu('');
        (isset($datas['Mai_Etu'])) ? $this->setMai_Etu($datas['Mai_Etu'] ): $this->setMai_Etu('');
        (isset($datas['Log_Etu'])) ? $this->setLog_Etu($datas['Log_Etu'] ): $this->setLog_Etu('');
        (isset($datas['Mdp_Etu'])) ? $this->setMdp_Etu($datas['Mdp_Etu'] ): $this->setMdp_Etu('');
        (isset($datas['Rem_Etu'])) ? $this->setRem_Etu($datas['Rem_Etu'] ): $this->setRem_Etu('');
        $date = DateTime::createFromFormat('j-M-Y', '15-Feb-2009');
        (isset($datas['Dat_Pnt_Tel_Etu'])) ? $this->setDat_Pnt_Tel_Etu($datas['Dat_Pnt_Tel_Etu'] ): $this->Dat_Pnt_Tel_Etu =null;
        (isset($datas['Suj_Etu'])) ? $this->setSuj_Etu($datas['Suj_Etu'] ): $this->setSuj_Etu('');
        (isset($datas['Id_Cla'])) ? $this->setId_Cla($datas['Id_Cla'] ): $this->Id_Cla=null;
        (isset($datas['Id_Spe'])) ? $this->setId_Spe($datas['Id_Spe'] ): $this->Id_Spe=null;
        (isset($datas['Num_Tut'])) ? $this->setNum_Tut($datas['Num_Tut'] ): $this->Num_Tut=null;
        (isset($datas['Id_Sta'])) ? $this->setId_Sta($datas['Id_Sta'] ): $this->Id_Cla=null;
        (isset($datas['Id_Ent'])) ? $this->setId_Ent($datas['Id_Ent'] ): $this->Id_Ent=null;
        (isset($datas['Note_Etu'])) ? $this->setNote_Etu($datas['Note_Etu'] ): $this->Note_Etu=null;
    }

}

}


?>