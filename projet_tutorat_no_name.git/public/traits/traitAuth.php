<?php

require_once '../../config/globalConfig.php';
require_once '../../config/appConfig.php';
session_start();


// Si l'accès ne se fait pas par une requête POST, on redirige vers la page d'accueil.
if('POST' === filter_input(INPUT_SERVER, 'REQUEST_METHOD', FILTER_SANITIZE_STRING))
{
    dump_var($_POST, DUMP, '$_POST brut');
    //  Filtrage des données venant du formulaire.
    $filters = array(
        'Num_Tut' => "",
        'Nom_Tut' => "",
        'Pre_Tut' => "",
        
	'Log_Tut' => FILTER_SANITIZE_STRING,
	'Mdp_Tut' => FILTER_SANITIZE_STRING,
    );
    $datas = filter_input_array(INPUT_POST, $filters);
    dump_var($datas, DUMP, '$_POST filtré');
    
    //  Instanciation d'un objet User avec les données filtrées
    $userAuth = new Entities\Tuteur($datas);
    
    $db = connectBdd($infoBdd);
    //  Instanciation du repository et authentification de la personne
    $repo = new Repositories\TuteurRepository($db);
    $user = $repo->Authentify($userAuth);
    
    dump_var($user, DUMP, '$user');
    dump_var($_SESSION, DUMP, '$_SESSION');
    
    
    if($user !== NULL)
    {
	//  $pers n'est pas null, l'authentification a réussi. Retour à l'accueil après 1 seconde.
	echo '<p>Authentification réussie</p>';
	        header('location: ../infosUtilisateur.php'); 
    }
    else
    {
	//  $pers est null, l'authentification a échoué. Retour au formulaire après 2 secondes.
	echo"<p>Erreur de login ou de mot de passe.</p>";
	header('location: ../index.php');
    } 
}
else
    header("location: ../index.php");

