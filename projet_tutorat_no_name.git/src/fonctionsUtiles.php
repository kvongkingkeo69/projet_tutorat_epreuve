<?php

use Entities\Tuteur;
use Entities\Entite_personne_contact;

require_once '../config/globalConfig.php';
require_once '../config/appConfig.php';



function connectBdd(array $infoBdd): ?\PDO {
    $db = null;

    // Récupération des informations de votre table infoBdd
$myport = ($infoBdd['port']);
$mycharset = ($infoBdd['charset']);
$hostname = ($infoBdd['host']);
$mydbname = ($infoBdd['dbname']);
$myusername = ($infoBdd['user']);
$mypassword = ($infoBdd['pass']);
    //  Composition du DSN
$dsn = "mysql:dbname=$mydbname;host=$hostname;port=$myport;charset=$mycharset";
    //  Instanciation de PDO (le \ pour l'espace de nom racine... peut être utile par la suite)
$db = new \PDO($dsn, $myusername, $mypassword);
    // renvoi de votre object PDO
    return $db;
}




function getEtudiantId(PDO $bdd, int $id): ?array {
    $resultSet = NULL;
    $query = 'SELECT * FROM Etudiant WHERE Num_Etu=:id;';
    $reqPrep = $bdd->prepare($query);
    $res = $reqPrep->execute([':id' => $id]);

    if ($res) {
	 while($tmp = $reqPrep->fetch(\PDO::FETCH_ASSOC)){
          $resultSet[] = $tmp ;
     }
    }
    return $resultSet;

}


function getTuteurId(PDO $bdd, int $id): ?array {
    $resultSet = NULL;
    $query = 'SELECT * FROM Tuteur WHERE Num_Tut=:id;';
    $reqPrep = $bdd->prepare($query);
    $res = $reqPrep->execute([':id' => $id]);

    if ($res) {
	 while($tmp = $reqPrep->fetch(\PDO::FETCH_ASSOC)){
          $resultSet[] = $tmp ;
     }
    }
    return $resultSet;

}

function getEntrepriseId(PDO $bdd, int $id): ?array {
    $resultSet = NULL;
    $query = 'SELECT * FROM Entreprise WHERE id_ent=:id;';
    $reqPrep = $bdd->prepare($query);
    $res = $reqPrep->execute([':id' => $id]);

    if ($res) {
	 while($tmp = $reqPrep->fetch(\PDO::FETCH_ASSOC)){
          $resultSet[] = $tmp ;
     }
    }
    return $resultSet;

}

function getAllPersonneContact(\PDO $bdd): ?array {
    $resultSet = NULL;
    $query = 'SELECT * FROM personne_contact ';

    $rqtResult = $bdd->query($query);

    if ($rqtResult) {
	while ($row = $rqtResult->fetch(\PDO::FETCH_ASSOC)) {
	    $resultSet[] = $row;
	}
    }
    return $resultSet;
}





function getTuteur(\PDO $bdd): ?array {
    $resultSet = NULL;
    $query = 'SELECT * FROM tuteur WHERE validation = 1';

    $rqtResult = $bdd->query($query);

    if ($rqtResult) {
	while ($row = $rqtResult->fetch(\PDO::FETCH_ASSOC)) {
	    $resultSet[] = $row;
	}
    }
    return $resultSet;
}


function insertBilan(\PDO $bdd, array $bilan): ?array {
    dump_var($bilan, DUMP, '$oeuvre dans insertBilan');
    $resultSet = NULL;

     $resultSet = NULL;
     $query = "INSERT INTO bilan" .
	    "(Dat_Bil, Not_Ent_Bil, Not_Dos_Bil, Not_Ora_Bil, Rem_Bil)"
	    . "VALUES (".$bdd->quote($bilan['Dat_Bil']).",".$bdd->quote($bilan['Not_Ent_Bil']).",".$bdd->quote($bilan['Not_Dos_Bil']).",".$bdd->quote($bilan['Not_Ora_Bil']).",".$bdd->quote($bilan['Rem_Bil']).");";
dump_var($query, DUMP, '$query dans insertbilan');
     $bdd->exec($query);
     $id = $bdd->lastInsertId();

     $bilan['Id_Bil']=$id;
    return $bilan;
}
/*public function Authentify(Tuteur $entity){
	$resultSet = NULL;

	if ($entity != NULL) {
	    if ($this->bdd) {
		//  Conversion de l'entité en tableau associatif (nécesasire pour le binding)
		$query = 'SELECT * FROM Tuteur WHERE Log_Tut = :login';

		//  Préparation et exécution de la requête
		$reqPrep = $this->bdd->prepare($query);
		$reqPrep->bindValue(':login', $entity->getLog_Tut());
		$rqtResult = $reqPrep->execute();
		dump_var($rqtResult, DUMP, 'TuteurRepository, Authentify, $rqtResult');

		if ($rqtResult) {
		    //  Récupération des résultats d'exécution de la requête
		    $row = $reqPrep->fetch(\PDO::FETCH_ASSOC);
		    dump_var($row, DUMP, 'TuteurRepository, Authentify, $row');

		    if ($row) {
			//  On a un résultat
			$user = new Tuteur($row);
			dump_var($user, DUMP, 'TuteurRepository, Authentify, $user');

			//  Vérification du mot de passe.
			if (password_verify($entity->getMdp_Tut(), $user->getMdp_Tut())) {

			    //  On ne conserve nulle part le password en dehors de la bdd...
			    $user->setMdp_Tut();
                            dump_var($user, DUMP, 'TuteurRepository, mdp, $user');
			    //  Mise en place des variables de session

			    $_SESSION['Nom_Tut'] = $user->getNom_Tut();
			    $_SESSION['Pre_Tut'] = $user->getPre_Tut();

			    //  Authentification réussie, on retournera TRUE
			    $resultSet = TRUE;
			} else
			    $resultSet = FALSE;
		    } else
			$resultSet = FALSE;
		} else
		    $resultSet = FALSE;
	    } else
		$resultSet = FALSE;
	}
	return $resultSet;
    }*/