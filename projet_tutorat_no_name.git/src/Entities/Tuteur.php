<?php

namespace Entities;

class Tuteur 
{
    private int $num_tut ;
    private string $nom_tut;
    private string $pre_tut;
    private string $tel_tut;
    private string $mai_tut;
    private string $log_tut;
    private string $mdp_tut;
    private bool $cpt_val;
    private int $nb_etu_max_3ole;
    private int $nb_etu_max_4ole;
    private int $nb_etu_max_5ole;
    private int $id_pro;
    private int $validation; 
    
   public function getNum_tut () 
    {
        return $this -> num_tut;
    }
    
    public function getNom_tut ()  
    {
        return $this -> nom_tut;
    }
    
    public function getPre_tut ()   
    {
        return $this -> pre_tut;
    }
    
    public function getTel_tut ()
    {
        return $this-> tel_tut;
    }
    
    public function getMai_tut ()   
    {
        return $this->mai_tut;
    }
    
    public function getLog_tut ()    
    {
        return $this->log_tut;
    }
    
    public function getMdp_tut ()  
    {
        return $this->mdp_tut;
    }
    
    public function getCpt_val () 
    {
        return $this->cpt_val;
    }
    
    public function getNb_etu_max_3ole () 
    {
        return $this->nb_etu_max_3ole;
    }
    
    public function getNb_etu_max_4ole () 
    {
        return $this->nb_etu_max_4ole;
    }

    public function getNb_etu_max_5ole () 
    {
        return $this->nb_etu_max_5ole;
    }    
    
    public function getId_Pro()  {
        return $this -> id_pro;
    }

    public function getValidation() {
        return $this -> validation;
    }

    public function setNum_tut()
    {
        $this->num_tut = $num_tut; 
    }
    
    public function setNom_tut()
    {
        $this->nom_tut = $nom_tut;
    }
    
    public function setTel_tut()
    {
        $this->tel_tut = $tel_tut;
    }
    
    public function setMai_tut()
    {
        $this->mai_tut = $mai_tut;
    }
    

    public function setLog_tut()
    {
        $this->log_tut = $log_tut;
    }
    public function setMdp_tut()
    {
        $this->mdp_tut = $mdp_tut;
    }
    
    public function setCpt_val()
    {
        $this->cpt_val = $cpt_val;
    }
    
    public function setNb_etu_max_3ole()
    {
        $this-> nb_etu_max_3ole = $nb_etu_max_3ole;
    }
    
    
    public function setNb_etu_max_4ole()
    {
        $this-> nb_etu_max_3ole = $nb_etu_max_4ole;
    }
    
    public function setNb_etu_max_5ole()
    {
        $this-> nb_etu_max_5ole = $nb_etu_max_5ole;
    }

    public function setId_Pro() {
        $this -> id_pro = $id_pro;

    }

    public function setValidation() {
        $this -> validation = $validation;

    }
    
    public function __construct(?array $datas = null) 
    {
        if(!is_null($datas)){
            (isset($datas['Num_Tut'])) ? $this->setNum_tut($datas['Num_Tut'] ): $this->Num_Tut=null;
            (isset($datas['Nom_Tut'])) ? $this->setNom_tut($datas['Nom_Tut'] ): $this->setNom_Tut('');
            (isset($datas['Pre_Tut'])) ? $this->setPre_Tut($datas['Pre_Tut'] ): $this->setPre_Tut('');
            (isset($datas['Tel_Tut'])) ? $this->setTel_Tut($datas['Tel_Tut'] ): $this->setTel_Tut('');
            (isset($datas['Mai_Tut'])) ? $this->setMai_Tut($datas['Mai_Tut'] ): $this->setMai_Tut('');
            (isset($datas['Log_Tut'])) ? $this->setLog_Tut($datas['Log_Tut'] ): $this->setLog_Tut('');
            (isset($datas['Mdp_Tut'])) ? $this->setMdp_Tut($datas['Mdp_Tut'] ): $this->setMdp_Tut('');
            (isset($datas['Cpt_Val'])) ? $this->setCpt_val($datas['Cpt_Val'] ): $this->setCpt_val('');
            (isset($datas['Nb_Etu_Max_3OLE'])) ? $this->setNb_etu_max_3ole($datas['Nb_Etu_Max_3OLE'] ): $this->setNb_etu_max_3ole('');
            (isset($datas['Nb_Etu_Max_4OLE'])) ? $this->setNb_etu_max_4ole($datas['Nb_Etu_Max_4OLE'] ): $this->setNb_etu_max_4ole('');
            (isset($datas['Nb_Etu_Max_5OLE'])) ? $this->setNb_etu_max_5ole($datas['Nb_Etu_Max_5OLE'] ): $this->setNb_etu_max_5ole('');
            (isset($datas['id_pro'])) ? $this->setId_Pro($datas['id_pro'] ): $this->setId_Pro('');
            (isset($datas['validation'])) ? $this->setValidation($datas['validation'] ): $this->setValidation('');
            
        }
    
    
}

}






?>
