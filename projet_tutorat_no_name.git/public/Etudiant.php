<?php
	declare(strict_types=1);
	require_once '../config/appConfig.php';
	require_once '../src/fonctionsUtiles.php';

	$bdd = connectBdd($infoBdd);

	if ($bdd){
		$lesEtudiants = getEtudiantId($bdd, (int)$_GET['id']);
	}else{
		$lesEtudiants = null;
	}
?>

<!DOCTYPE html>
<HTML>
	<HEAD> 
		<TITLE> 2 sio projet tutorat </TITLE>
        <meta charset="UTF-8">
		<link rel="stylesheet" href="css/style.css">
		<?php
            include_once 'inc/header.php';
            include_once 'inc/menu.php';
            
            ?>
	</HEAD>
	<BODY>
		
       
		<section>
            <h1>LISTES DES Etudiants </h1>
           
                <?php if (!is_null($lesEtudiants)): ?>
    			    <table>
						<thead>
								<tr>
									<th> Nom </th>
									<th> Prénom </th>
									<th> téléphone </th>
									<th> mail </th>
									<th> spécialité </th>
									<th> statut </th>
									<th> remarques </th>
									<th> date du point téléphonique </th>
									<th> sujet d'analyse / sujet mémoire  </th>
									<th>Note</th>
									<th> bilan 1 </th>
									<th> bilan 2 </th>
								</tr>
						</thead>
						<tbody>
							<?php foreach ($lesEtudiants as $Etudiant):	?>
								<tr>
									<td><?= $Etudiant['Nom_Etu'] ?></td>
									<td><?= $Etudiant['Pre_Etu'] ?></td>
									<td><?= $Etudiant['Tel_Etu'] ?></td>
									<td><?= $Etudiant['Mai_Etu'] ?></td>
									<td><?= $Etudiant['Id_Spe'] ?></td>
									<td><?= $Etudiant['Id_Sta']  ?></td>
									<td><?= $Etudiant['Rem_Etu'] ?></td>
									<td><?= $Etudiant['Dat_Pnt_Tel_Etu'] ?></td>
									<td><?= $Etudiant['Suj_Etu'] ?></td>
									<td><?= $Etudiant['Note_Etu'] ?></td>
									<td> <a href="bilan1.php?id=<?= $Etudiant['Num_Etu'] ?>"><img id="im" src="../public/img/edit.png"></a></td>
									<td> <a href="bilan2.php?id=<?= $Etudiant['Num_Etu'] ?>"><img id="im" src="../public/img/edit.png"></a></td>
								</tr>
							<?php endforeach; ?>
						</tbody>
    			    </table>
			    <?php else: ?>
    			    <p>Oups... Il semble y avoir eu une erreur!</p>
			    <?php endif; ?>
        </section>
	
    </body>
	<?php
            include_once 'inc/footer.php';
        ?>
        </div>
</html>