<!DOCTYPE html>
<?php  
require_once '../config/globalConfig.php';
require_once '../config/appConfig.php';
require_once '../src/fonctionsUtiles.php';
?>
<HTML>
	<HEAD>
		<TITLE> Créer un compte </TITLE>
                <meta charset="UTF-8">
                <link rel="stylesheet" media="screen"type="text/css" href="css/style.css">	

	</HEAD>
	<BODY>
        <?php
            include_once 'inc/header.php';
            include_once 'inc/menu.php';
            
            ?>

<center>				   
	<section id="corps">
            <h1>Créer un compte</h1>
            <form method="post" action="../public/traitAjoutCompte.php">
                <div>
                    <label for="Nom_Etu">Nom :</label><br/>
                    <input type="text" id="Nom_Etu" placeholder="Nom de l'utilisateur" name="Nom_Etu"  required="required"  autocomplete="off" size="40">
                </div>
                <div>
                    <label for="Pre_Etu">Prénom :</label><br/>
                    <input type="text" id="Pre_Etu" placeholder="Prénom de l'utilisateur" name="Pre_Etu" required="required"  autocomplete="off"size="40">
		</div>
		<div>
                    <label for="Log_Etu">Login :</label><br/>
                    <input type="text" id="loginUti" placeholder="Login de l'utilisateur" name="Log_Etu" required="required"  autocomplete="off"size="40">
		</div>
		<div>
                    <label for="Mdp_Etu">Mot de passe :</label><br/>
                    <input type="password" id="Mdp_Etu" placeholder="Mot de passe" name="Mdp_Etu" size="40" required="required" autocomplete="off">
		</div>
		<br/>
		<div class="form-group">
			<button type="submit">Créer</button>
		</div>
            </form>
	</section></center>
	<?php
            include_once 'inc/footer.php';
        ?>
        </div>
    </BODY>
</HTML>