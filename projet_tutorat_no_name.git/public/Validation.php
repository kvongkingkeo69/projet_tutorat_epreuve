<?php
declare(strict_types=1);
require_once '../config/appConfig.php';
require_once '../src/fonctionsUtiles.php';

$bdd = connectBdd($infoBdd);

if ($bdd){
    $tuteur = getTuteur($bdd);
	
}
else {
     $tuteur = null;
}?>

<!DOCTYPE html>
<HTML>
	<HEAD> 
		<TITLE> 2 sio projet tutorat </TITLE>
                <meta charset="UTF-8">
				<link rel="stylesheet" href="css/style.css">
				<?php
            include_once 'inc/header.php';
            include_once 'inc/menu.php';
        ?>
	</HEAD>
	<BODY>
       
    <section>
            <h1>page de validation </h1>
           
              <?php if (!is_null($tuteur)): ?>
    			    <table>
    				<thead>
                        <center>
                            <tr>    
									<th> Nom </th>
									<th> Prénom </th>
									<th> téléphone </th>
									<th> mail </th>
									<th> login </th>
									<th> mot de passe hashé</th>
                                    <th> Valider </th>
                                    <th> Réfuser </th>
								</tr>
                        </center>                         
    				</thead>
    				<tbody>
				    <?php foreach ($tuteur as $inscrip):?>
					<tr> 
                        
					    <td><?= $inscrip['Nom_Tut'] ?></td>
					    <td><?= $inscrip['Pre_Tut'] ?></td>
						<td><?= $inscrip['Tel_Tut'] ?></td>
						<td><?= $inscrip['Mai_Tut'] ?></td>
						<td><?= $inscrip['Log_Tut'] ?></td>
					    <td><?= $inscrip['Mdp_Tut']  ?></td>
                        <td><a href="accepter.php?id=<?=$inscrip['Num_Tut']; ?>"><span>&#10003;</span></a></td>
                        <td><a href="refuser.php?id=<?=$inscrip['Num_Tut']; ?>"><span>&#215;</span></a></td>
					</tr>
				    <?php endforeach; ?>
    				</tbody>
    			    </table>
			    <?php else: ?>
    			    <p>Oups... Il semble y avoir eu une erreur!</p>
			    <?php endif; ?>


        </section>
		<?php
            include_once 'inc/footer.php';
        ?>
    </body>
</html>