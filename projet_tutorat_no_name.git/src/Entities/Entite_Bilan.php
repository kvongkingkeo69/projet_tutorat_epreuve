<?php
namespace Entities;

class Bilan 
{
    private $id_bil;
    private $dat_bil;
    private $not_ent_bil;
    private $not_ora_bil;
    private $not_dos_bil;
    private $not_ora_fin_bil;
    private $not_dos_fin_bil;
    private $rem_bil;
    private $num_bil;
    private $num_etu;

    function getId_Bil() : int
{
    return $this -> id_bil;
}

function getDat_Bil() : date 
{
    return $this -> dat_bil;
}

function getNot_Ent_Bil() : float
{
    return $this -> not_ent_bil;
    
}

function getNot_Ora_Bil() : float
{
    return $this->not_ora_bil;
}

function getNot_Dos_Bil() : float
{
    return $this->not_dos_bil;
}

function getNot_Ora_Fin_Bil() : float
{
    return $this->not_ora_fin_bil;
}

function getNot_Dos_Fin_Bil() : float
{
    return $this-> not_dos_fin_bil;
}

function getRem_Bil() : float
{
    return $this->rem_bil;
}
function getNum_Bil() : ?int
{
    return $this->num_bil;
}

function getNum_Etu() : ?int
{
    return $this->num_etu;
}





function setId_Bil()
{
 if ($this->id_bil == null){
            $this->id_bil = ($tmp = filter_var($id_bil, FILTER_VALIDATE_INT)) ? $tmp : null;
        }
        
        return $this;
}

function setDat_Bil()
{
    $this->dat_bil = $dat_bil;
    return $this;
}

function setNot_Ent_Bil()
{
    $this -> not_ent_bil = $not_ent_bil;
}

function setNot_Ora_Bil()
{
    $this->not_ora_bil = $not_ora_bil;
}

function setNot_Dos_Bil()
{
    $this->not_dos_fin_bil = $not_dos_fin_bil;
}

function setNot_Ora_Fin_Bil()
{
    $this -> not_ora_fin_bil = $not_ora_fin_bil; 
}

function setRem_Bil()
{
        $this -> rem_bil = $rem_bil;
}

function setNum_Bil(int $num_bil): self {
        $this->num_bil= $num_bil;
        return $this;
    }
function setNum_Etu(int $num_etu): self {
        $this->num_etu= $num_etu;
        return $this;
    }
    
function __construct(?array $datas = null) 
{
    $this->id_bil = $datas['id_bil'];
    $this->dat_bil = $datas['dat_bil'];
    $this-> not_ent_bil= $datas['not_ent_bil'];
    $this->not_ora_bil= $datas['not_ora_bil'];
    $this->not_dos_bil= $datas['not_dos_bil'];
    $this->not_ora_fin_bil = $datas['not_ora_fin_bil'];
    $this->not_dos_fin_bil = $datas['not_dos_fin_bil'];
    $this->rem_bil = $datas['rem_bil'];
    $this->num_bil = $datas['num_bil'];
    $this->num_etu = $datas['num_etu'];


    

    
}
}


?>
