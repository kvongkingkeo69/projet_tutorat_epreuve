<?php
//  Les inclusions nécessaires
require_once '../config/globalConfig.php';
require_once '../config/appConfig.php';
require_once '../src/fonctionsUtiles.php';



if ('POST' === $_SERVER['REQUEST_METHOD']) {

    dump_var($_POST, DUMP, '$_POST');

     $filtres =array (
        'Nom_Tut'=>FILTER_SANITIZE_STRING,
        'Pre_Tut'=>FILTER_SANITIZE_STRING,
        'Log_Tut'=> FILTER_SANITIZE_STRING,
        'Mdp_Tut'=> FILTER_SANITIZE_STRING,
    );

    $postFiltre = filter_input_array(INPUT_POST, $filtres, TRUE);

    $motDePasse = password_hash($postFiltre['Mdp_Tut'], PASSWORD_DEFAULT);

    $postFiltre['Mdp_Tut']=$motDePasse;

    dump_var($postFiltre, DUMP, '$postFiltre');
    $bdd = connectBdd($infoBdd);
    dump_var($bdd, DUMP, '$bdd');
    if ($bdd) {
        $newUser = new Entities\Tuteur($postFiltre);
        dump_var($newUser, DUMP, '$newUser');
        $repo = new Repositories\TuteurRepository($bdd);
        $user = $repo->ajoutTuteur($newUser);
        dump_var($user, DUMP, '$user');
        if ($user){

            header('location: ../infosUtilisateur.php');
        }
          else {
              header('location: ../creerCompte.php');
          }

    }


}