<!DOCTYPE html>

<html lang="fr">

<head>

    <meta charset="UTF-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" media="screen"type="text/css" href="css/style.css">	
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php
            include_once 'inc/header.php';
            include_once 'inc/menu.php';
            
            ?>
    <title>STATISTIQUES</title>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.css" integrity="sha512-/zs32ZEJh+/EO2N1b0PEdoA10JkdC3zJ8L5FTiQu82LR9S/rOQNfQN7U59U9BC12swNeRAz3HSzIL2vpp4fv3w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" href="css/css.css"/>
</head>



<body>

    <canvas id="monGraph" width="400" height="100"></canvas>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js" integrity="sha512-s+xg36jbIujB2S2VKfpGmlC3T5V2TF3lY48DX7u2r9XzGzgPsa6wTpOQA7J9iffvdeBN0q9tKzRxVxw1JviZPg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>



<script>

        var ctx = document.querySelector("#monGraph")

        var graph = new Chart(ctx, {

            type: 'line',

        data: {

        labels: ["bilan 1 entreprise", "bilan 1 CRA", "Bilan 1 Oral"],

        datasets: [{

            label: 'Moyenne des bilans',

            data: [12, 19, 3, 0, 20, 3],

            backgroundColor: [

                'rgba(255, 99, 132, 0.2)',

                'rgba(54, 162, 235, 0.2)',

                'rgba(255, 206, 86, 0.2)',

                'rgba(75, 192, 192, 0.2)',

                'rgba(153, 102, 255, 0.2)',

                'rgba(255, 159, 64, 0.2)'

            ],

            borderColor: [

                'rgba(255, 99, 132, 1)',

                'rgba(54, 162, 235, 1)',

                'rgba(255, 206, 86, 1)',

                'rgba(75, 192, 192, 1)',

                'rgba(153, 102, 255, 1)',

                'rgba(255, 159, 64, 1)'

            ],

           

            borderWidth: 1

        }]

       

    },

    options: {

        scales: {

            y: {

                beginAtZero: true

            }

        }

    }

});

</script>
</body>
<?php
            include_once 'inc/footer.php';
        ?>
</html>
