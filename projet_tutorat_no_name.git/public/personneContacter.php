<?php
declare(strict_types=1);
require_once '../config/appConfig.php';
require_once '../src/fonctionsUtiles.php';

$bdd = connectBdd($infoBdd);



if ($bdd){
	$repo = new Repositories\RepoPersonneContact($bdd);
	$personneContacts = $repo->getAllPersonneContact();

}
else {
     $personneContacts = null;
}

?>

<!DOCTYPE html>
<HTML>
	<HEAD> 
		<TITLE> 2 sio projet tutorat </TITLE>
        <meta charset="UTF-8">
		<link rel="stylesheet" href="css/style.css">
				

	</HEAD>
	<BODY >
       
		<?php
            include_once 'inc/header.php';
            include_once 'inc/menu.php';
        ?>
			<section>
					<h1>Liste des personnes à contacter  </h1>
				
					<?php if (!is_null($personneContacts)): ?>
							<table>
							<thead>
										<tr>
											<th> id</th>
											
											<th> Nom </th>
											<th> Prénom </th>
											<th> téléphone </th>
											<th> mail </th>
											
										</tr>
							</thead>
							<tbody>
							<?php
							foreach ($personneContacts as $personneContact):
							?>
							<tr>
								<td>1</td>
								<td> <a href="personneContact.php?id=<?=$personneContact->getNum_Etu();?>"><img  id="im" width="10px" src="../public/img/check.svg.png"></a></td>
								<td><?= $personneContact->getId_Contact(); ?></td>
								<td><?= $personneContact->getPre_contact(); ?></td>
								<td><?= $personneContact->getNom_contact(); ?></td>
								<td><?= $personneContact->getTel_Contact();?></td>
								<td><?= $personneContact->getMai_contact(); ?></td>
								
		
							</tr>
							<?php endforeach; ?>
							</tbody>
							</table>
						<?php else: ?>
							<p>Oups... Il semble y avoir eu une erreur!</p>
						<?php endif; ?>
			</section>
	
    </body>
</html>