<?php


declare(strict_types=1);


define('DUMP', true);

require_once '../config/appConfig.php';
require_once '../src/fonctionsUtiles.php';

$db = connectBdd($infoBdd);

if (!is_null($db)) {
    $res = getEntrepriseId($db, 1);
    dump_var($res, DUMP, 'getEntrepriseId(1):');
} else {
    echo '<h1>Erreur de création de la connexion $db</h1>';
}