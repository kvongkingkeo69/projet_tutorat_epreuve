<?php
declare(strict_types=1);
require_once '../config/appConfig.php';
require_once '../src/fonctionsUtiles.php';

$bdd = connectBdd($infoBdd);



if ($bdd){
	$repo = new Repositories\EtudiantRepository($bdd);
	$lesEtudiants = $repo->getAlletudiant5OLEN();

}
else {
     $lesEtudiants = null;
}

?>

<!DOCTYPE html>
<HTML>
	<HEAD> 
		<TITLE> 2 sio projet tutorat </TITLE>
        <meta charset="UTF-8">
		<link rel="stylesheet" href="css/style.css">
				

	</HEAD>
	<BODY >
       
		<?php
            include_once 'inc/header.php';
            include_once 'inc/menu.php';
        ?>
			<section>
					<h1>LISTES DES Etudiants </h1>
				
					<?php if (!is_null($lesEtudiants)): ?>
							<table>
							<thead>
										<tr>
											<th> numemro</th>
											<th> Selectionner </th>
											<th> Nom </th>
											<th> Prénom </th>
											<th> téléphone </th>
											<th> mail </th>
											<th> spécialité </th>
											<th> statut </th>
											<th> remarques </th>
											<th> date du point téléphonique </th>
											<th> sujet d'analyse / sujet mémoire  </th>
											<th>Note</th>
										</tr>
							</thead>
							<tbody>
							<?php
							foreach ($lesEtudiants as $Etudiant):
							?>
							<tr>
								<td>1</td>
								<td> <a href="Etudiant.php?id=<?=$Etudiant->getNum_Etu();?>"><img  id="im" width="10px" src="../public/img/check.svg.png"></a></td>
								<td><?= $Etudiant->getNom_Etu(); ?></td>
								<td><?= $Etudiant->getPre_Etu(); ?></td>
								<td><?= $Etudiant->getTel_Etu();?></td>
								<td><?= $Etudiant->getMai_Etu(); ?></td>
								<td><?= $Etudiant->getId_Spe(); ?></td>
								<td><?= $Etudiant->getId_Sta(); ?></td>
								<td><?= $Etudiant->getRem_Etu(); ?></td>
								<td><?= $Etudiant->getDat_Pnt_Tel_Etu(); ?></td>
								<td><?= $Etudiant->getSuj_Etu(); ?></td>
								<td><?= $Etudiant->getNote_Etu(); ?></td>
							</tr>
							<?php endforeach; ?>
							</tbody>
							</table>
						<?php else: ?>
							<p>Oups... Il semble y avoir eu une erreur!</p>
						<?php endif; ?>
			</section>
	
    </body>
</html>