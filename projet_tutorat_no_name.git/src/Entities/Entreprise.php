<?php 

namespace Entities; 

class Entreprise 
{
    private $id_ent;
    private $lib_ent;
    private $adr_ent;
    private $cp_ent;
    private $vil_ent;
    private $nom_mai_app;
    private $pre_mai_app;
    private $tel_mai_app;

    function getId_Ent() : int
    {
        return $this->id_ent;
    }
    
    function getLib_ent() : string
    {
        return $this->lib_ent;
    }
    
    function getAdr_ent() : string 
    {
        return $this->adr_ent;
    }
    
    function getCp_ent() : int
    {
        return $this->cp_ent;
    }
    
    function getVil_ent() : string 
    {
        return $this->vil_ent;
    }

    
    function setId_ent()
    {
        $this->id_ent = $id_ent; 
    }
    
    function setLib_ent()
    {
        $this->lib_ent = $lib_ent;
    }
    
    function setAdr_ent()
    {
        $this->adr_ent = $adr_ent;
    }
    
    function setCp_ent()
    {
        $this->cp_ent = $cp_ent;
    }
    
    function setVil_ent()
    {
        $this->vil_ent = $vil_ent;
    }

    
   
    function __construct(?array $datas = null){
        
        if(!is_null($datas)){
            (isset($datas['id_ent'])) ? $this->setId_ent($datas['id_ent'] ): $this->id_ent=null;
            (isset($datas['lib_ent'])) ? $this->setLib_ent($datas['lib_ent'] ): $this->setLib_ent('');
            (isset($datas['adr_ent'])) ? $this->setAdr_ent($datas['adr_ent'] ): $this->setAdr_ent('');
            (isset($datas['cp_ent'])) ? $this->setCp_ent($datas['cp_ent'] ): $this->setCp_ent('');
            (isset($datas['vil_ent'])) ? $this->setVil_ent($datas['vil_ent'] ): $this->setVil_ent('');
            
    }
}

}

?>
