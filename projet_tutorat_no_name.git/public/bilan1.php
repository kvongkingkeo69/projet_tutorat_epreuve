<?php
declare(strict_types=1);
require_once '../config/appConfig.php';
require_once '../src/fonctionsUtiles.php';

$bdd = connectBdd($infoBdd);
if ($bdd) {
    $lesEtudiants = getAlletudiant($bdd);
} else {
    $lesEtudiants = null;
}
?>



<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <?php
            include_once 'inc/header.php';
            include_once 'inc/menu.php';
            
            ?>
    <title>Document</title>
</head>
<body>

<center>
        <form  action="traits/traitajoutbilan1.php" method="post">
            <fieldset>
                <legend>
                    le bilan 1
                </legend>
                <label> Date de la visite en entreprise </label>
                <input type="date" name="Dat_Bil"></br>

                <label> la note de l'entreprise </label>
                <input type="number" name="Not_Ent_Bil"> </br>

                <label> la note du CRA </label>
                <input type="number" name="Not_Dos_Bil"></br>
            
                <label> la note d'oral </label>
                <input type="number" name="Not_Ora_Bil"> <br>

                <label> Remarque </label>
                <input type="text-area" name="Rem_Bil">

                <input type="submit" value="Valider le bilan"> <br>
            </fieldset>
    </form> 
    </center>    
</body>
<?php
            include_once 'inc/footer.php';
        ?>
        </div>
</html>