<?php
declare(strict_types=1);

require_once '../../config/appConfig.php';
require_once '../../src/fonctionsUtiles.php';

$error = 0;

if ('POST' === $_SERVER['REQUEST_METHOD']) {

    dump_var($_POST, DUMP, '$_POST');
    
    
    $filters = array(
	
	'Dat_Bil' => FILTER_SANITIZE_STRING,
	'Not_Ent_Bil' => FILTER_VALIDATE_INT,
	'Not_Dos_Bil' => FILTER_VALIDATE_INT,	
	'Not_Ora_Bil' => FILTER_VALIDATE_INT,
	'Rem_Bil' => FILTER_SANITIZE_STRING,
    );

    //  Vérification des types
    $postFiltre = filter_input_array(INPUT_POST, $filters, TRUE);
    dump_var($postFiltre, DUMP, '$postFiltre');
    

	$bdd = connectBdd($infoBdd);
	if ($bdd) {
	    $bilan = insertBilan($bdd, $postFiltre);
	    dump_var($bilan, DUMP, '$bilan');
	}
	else {
	    $error = -3;
	}
    
}
else {
    $error = -1;
}
