<?php

namespace Repositories;

use Entities\Etudiant;


class EtudiantRepository {
    
    protected $bdd;

    public function __construct(\PDO $bdd){
        if (!is_null($bdd)) {
            $this->bdd = $bdd;
        }
    }
    
	public function getAll(): ? array {

        $resultSet = NULL;
        $query = 'SELECT * FROM Etudiant';
    

        $rqtResult = $this->bdd->query($query);
     
        if ($rqtResult){
            $rqtResult->setFetchMode(\PDO::FETCH_ASSOC);
            foreach($rqtResult as $row){
                $resultSet[] = new Etudiant($row);
            }
         
        
        }
        return $resultSet;
    }
 

    


public function getById( int $id): ?Etudiant {
        $resultSet = NULL;
        $query = 'SELECT * FROM etudiant;';

        // On prépare la rêquete
        $reqPrep = $this->bdd->prepare($query);

        $res = $reqPrep->execute([':Num_Etu' => $id]);

        if ($res !== FALSE) {
            $tab = ($tmp = $reqPrep->fetch(\PDO::FETCH_ASSOC)) ? $tmp : null;
            if(!is_null($tab)) {
                // Si on récupère une occurence, on crée un objet acteur avec cette dernière
                $resultSet = new Etudiant($tab);
            }
        }
        return $resultSet;
    }


public function ajoutEtudiant(Etudiant $entity){
    $resultat = true;
    $query = "INSERT INTO etudiant" .
	    " (Nom_Etu,Pre_Etu,Log_Etu, Mdp_Etu)"
	    . " VALUES (:Nom_Etu, :Pre_Etu, :Log_Etu, :Mdp_Etu)";

    $reqPrep = $this->bdd->prepare($query);
    $res = $reqPrep->execute(
	    [':Nom_Etu'=>$entity->getNom_Etu(),
             ':Pre_Etu'=>$entity->getPre_Etu(),
             ':Log_Etu' => $entity->getLog_Etu(),
	     ':Mdp_Etu' => $entity->getMdp_Etu(),
	    ]
    );

    if ($res == FALSE) {
        $resultat = false;
        }
        else {
            
         $_SESSION['Nom_Etu'] = $entity->getNom_Etu();
         $_SESSION['Pre_Etu'] = $entity->getPre_Etu();
        }
}
public function getAlletudiant3OLEN(): ? array {

    $resultSet = NULL;
    $query = 'SELECT * FROM etudiant WHERE Id_Cla = 1';


    $rqtResult = $this->bdd->query($query);
 
    if ($rqtResult){
        $rqtResult->setFetchMode(\PDO::FETCH_ASSOC);
        foreach($rqtResult as $row){
            $resultSet[] = new Etudiant($row);
        }
     
    
    }
    return $resultSet;
}
public function getAlletudiant4OLEN(): ? array {

    $resultSet = NULL;
    $query = 'SELECT * FROM etudiant WHERE Id_Cla = 2';


    $rqtResult = $this->bdd->query($query);
 
    if ($rqtResult){
        $rqtResult->setFetchMode(\PDO::FETCH_ASSOC);
        foreach($rqtResult as $row){
            $resultSet[] = new Etudiant($row);
        }
     
    
    }
    return $resultSet;
}
public function getAlletudiant5OLEN(): ? array {

    $resultSet = NULL;
    $query = 'SELECT * FROM etudiant WHERE Id_Cla = 3';


    $rqtResult = $this->bdd->query($query);
 
    if ($rqtResult){
        $rqtResult->setFetchMode(\PDO::FETCH_ASSOC);
        foreach($rqtResult as $row){
            $resultSet[] = new Etudiant($row);
        }
     
    
    }
    return $resultSet;
}
}