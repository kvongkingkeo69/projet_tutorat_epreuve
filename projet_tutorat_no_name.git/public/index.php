<?php
session_start();
?>
<!DOCTYPE html>

<HTML>
	<HEAD>
		<TITLE>Connexion</TITLE>
                <meta charset="UTF-8">
		<link rel="stylesheet" media="screen"type="text/css" href="css/style.css">	

	</HEAD>
	<BODY>
        <?php
            include_once 'inc/header.php';
            include_once 'inc/menu.php';
        ?>
	<section id="corps">

               <article>
			<header>
			    <h1>Authentification</h1>

                           <?php if ($_SESSION!=null) {
                                ?>
                                <center><p>Vous êtes déjà connecter </p></center>
                                 <fieldset><legend>Vos infos:</legend>
				    <table>
					<tr><td class="alignRight"><?php echo $_SESSION['Num_Etu'] ?></td></tr>
					<tr><td class="alignRight"><?php echo $_SESSION['Pre_Etu']?></td></tr>
				    </table>
				</fieldset>
                           <?php
                            }
                            else{
                               ?>

			    <p>Connectez-vous avant de pouvoir accéder à l'application</p>
							</br></br>
			</header>
			<center><section>

			    <form method="post" action="traits/traitAuth.php">
				<fieldset><legend>Vos infos:</legend>
				    <table>
					<tr><td class="alignRight"><label for="login">Identifiant</label></td><td><input type="text" name="Log_Etu" id="login" required="required" size="64" autocomplete="off" maxlength="255" /></td></tr>
					<tr><td class="alignRight"><label for="mot de passe">Mot de passe</label></td><td><input type="password" name="Mdp_Etu" id="password" required="required" size="64" autocomplete="off" maxlength="255" /></td></tr>
				    </table>
				</fieldset>
				<input type="submit" class="small green" value="Connexion" />
			    </form>
			</section></center>
                            <?php
                            }
                            ?>

		    </article>

        </section>
	
        </div>
    </BODY>
	<?php
            include_once 'inc/footer.php';
        ?>
</HTML>

