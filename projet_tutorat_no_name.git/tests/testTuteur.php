<?php


declare(strict_types=1);


define('DUMP', true);

require_once '../config/appConfig.php';
require_once '../src/fonctionsUtiles.php';

$db = connectBdd($infoBdd);

if (!is_null($db)) {
    $res = getTuteurId($db, 1);
    dump_var($res, DUMP, 'getTuteur(1):');
} else {
    echo '<h1>Erreur de création de la connexion $db</h1>';
}