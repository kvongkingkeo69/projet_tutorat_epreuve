<?php

require_once '../../config/appConfig.php';
require_once '../../config/globalConfig.php';
require_once '../../src/fonctionsUtiles.php';

session_destroy();

    header("location: ../index.php");