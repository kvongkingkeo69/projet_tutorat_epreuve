<?php
//  Basculer à TRUE pour activer les affichages de debug, les var_dump ou les dump_var
if(!defined('DUMP')) {
    define('DUMP', false);
}

$infoBdd = array(
		'interface' => 'pdo',	    
		'type'   => 'mysql',	    
		'host'   => 'localhost',
		'port'   =>  3306,	    
		'charset' => 'UTF8',
    	'dbname' => 'bdd_tutorat', 
		'user'   => 'root',
		'pass'   => '',
		
	);
	
require_once 'globalConfig.php';