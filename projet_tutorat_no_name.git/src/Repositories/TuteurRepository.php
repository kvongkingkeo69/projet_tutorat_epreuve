<?php

namespace Repositories;

use Entities\Tuteur;


class TuteurRepository {
    
    protected $bdd;

    public function __construct(\PDO $bdd){
        if (!is_null($bdd)) {
            $this->bdd = $bdd;
        }
    }
    
	public function getAll(): ? array {

        $resultSet = NULL;
        $query = 'SELECT * FROM Tuteur';
    

        $rqtResult = $this->bdd->query($query);
     
        if ($rqtResult){
            $rqtResult->setFetchMode(\PDO::FETCH_ASSOC);
            foreach($rqtResult as $row){
                $resultSet[] = new Tuteur($row);
            }
         
        
        }
        return $resultSet;
    }
 
    public function Authentify() {
        
    }



public function ajoutTuteur(Tuteur $entity){
    $resultat = true;
    $query = "INSERT INTO tuteur" .
	    " (Nom_tut,Pre_Tut, Log_Tut, Mdp_Tut)"
	    . " VALUES (:Nom_tut, :Pre_tut, :Log_tut, :Mdp_tut)";

    $reqPrep = $this->bdd->prepare($query);
    $res = $reqPrep->execute(
	    [':Nom_tut'=>$entity->getNom_tut(),
             ':Pre_tut'=>$entity->getPre_tut(),
             ':Log_tut' => $entity->getLog_tut(),
	     ':Mdp_tut' => $entity->getMdp_tut(),
	    ]
    );

    if ($res == FALSE) {
        $resultat = false;
        }
        else {
            
         $_SESSION['Nom_Tut'] = $entity->getNom_Etu();
         $_SESSION['Pre_Tut'] = $entity->getPre_Etu();
        }
}


}