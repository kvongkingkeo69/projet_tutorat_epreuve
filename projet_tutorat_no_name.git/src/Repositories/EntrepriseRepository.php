<?php

namespace Repositories;

use Entities\Entreprise;


class EntrepriseRepository {
    
    protected $bdd;

    public function __construct(\PDO $bdd){
        if (!is_null($bdd)) {
            $this->bdd = $bdd;
        }
    }
    
	public function getAllEntreprises(): ? array {

        $resultSet = NULL;
        $query = 'SELECT * FROM Entreprise';
    

        $rqtResult = $this->bdd->query($query);
     
        if ($rqtResult){
            $rqtResult->setFetchMode(\PDO::FETCH_ASSOC);
            foreach($rqtResult as $row){
                $resultSet[] = new Entreprise($row);
            }
         
        
        }
        return $resultSet;
    }
 

    


public function getById( int $id): ?Entreprise {
        $resultSet = NULL;
        $query = 'SELECT * FROM entreprise where id_ent = :id_ent;';

        // On prépare la rêquete
        $reqPrep = $this->bdd->prepare($query);

        $res = $reqPrep->execute([':id_ent' => $id]);

        if ($res !== FALSE) {
            $tab = ($tmp = $reqPrep->fetch(\PDO::FETCH_ASSOC)) ? $tmp : null;
            if(!is_null($tab)) {
                // Si on récupère une occurence, on crée un objet acteur avec cette dernière
                $resultSet = new Entreprise ($tab);
            }
        }
        return $resultSet;
    }





     
    
    

}